#!/usr/bin/env node

const fs = require('fs');
const os = require('os');
const path = require('path');
const exec = require('child_process').exec;

const request = require('request');
const inline = require('web-resource-inliner');
const scrape = require('website-scraper');
const archiver = require('archiver');
const rimraf = require('rimraf');

const config = require('./config');
const decktape = '/decktape/decktape.js';

// TODO add config object as optional function parameter
// TODO rename slides to url
function exportslides(slides = '', filePath = 'slides.pdf', callback = EMPTY) {

    const isUrl = slides.startsWith('http');
    const isLocalFile = fs.existsSync(slides);

    if (!slides || (!isUrl && !isLocalFile)) throw 'Please supply a valid URL or file path of presentation to export.';

    const name = safeFilename(path.parse(filePath).name);
    const type = safeType(path.parse(filePath).ext.slice(1).toLowerCase());
    
    const workingDir = createTmpDir();
    const outputFile = path.join(workingDir, `${name}.${type}`);
    const targetDir = path.parse(filePath).dir || process.cwd();
    const targetFile = path.join(targetDir, `${name}.${type}`)

    if (isLocalFile) copyToWorkingDir(slides);

    if (type == 'pdf' || type == 'pptx') {
        
        const screenshotsDir = path.join(workingDir, 'screenshots');
        let screenshots = '';
        if (type == 'pptx') {
            fs.mkdirSync(screenshotsDir);
            screenshots = '--screenshots';
        }

        // decktape does not save screenshots correctly with full path to file
        let createPDFcmd = `cd ${workingDir}; \
                            node ${decktape} \
                            --chrome-path chromium-browser \
                            --chrome-arg=--no-sandbox \
                            ${screenshots} \
                            ${slides} ${name}.pdf`;

        exec(createPDFcmd, (error, stdout, stderr) => {
            console.log(stdout);
            console.log(stderr);
            if (error) throw `create pdf error: ${error}`;
            if (type == 'pdf') {
                finalize();

            } else if (type == 'pptx') {
                const pptxOtions = {
                    dir: screenshotsDir,
                }
                
                createpptx(outputFile, pptxOtions, () => {
                    finalize();
                });
            }
    
        });

    } else if (type == 'html') {

        if (isUrl) {
            request({uri: slides}, (error, response, body) => {
                if (error) throw `URL request error: ${error}`;
                inlineHtml(body);
            });
        } else {
            fs.readFile(slides, "utf8", (error, body) => {
                if (error) throw `HTML file read error: ${error}`;
                inlineHtml(body);
            });
        }

        function inlineHtml(body) {
            inline.html({
                fileContent: body,
                relativeTo: slides
            }, 
            (error, result) => {
                if (error) throw `HTML inline error: ${error}`;
                fs.writeFile(outputFile, result, () => {
                    finalize();
                });
            });
        }

    } else if (type == 'zip') {
        if (isLocalFile) {
            // TODO: This does not work!
        }

        const scrapeDir = path.join(workingDir, name);
        const scrapeOptions = {
            urls: [slides],
            directory: scrapeDir,
            subdirectories: [
                {
                    directory: `${name}_files`, 
                    extensions: ['.jpg', '.png', '.gif', '.svg', '.js', 'json', '.css'],
                },
                ],
            defaultFilename: `${name}.html`,
        };
        
        scrape(scrapeOptions, (error, result) => {
            if (error) throw `Scrape error: ${error}`;

            const zip = fs.createWriteStream(outputFile);
            const archive = archiver('zip');
            zip.on('close', finalize);
            archive.on('error', console.log);
            archive.pipe(zip);
            archive.directory(scrapeDir, false);
            archive.finalize();

        });
    }

    function copyToWorkingDir(file) {
        fs.copyFileSync(file, path.join(workingDir, path.parse(file).base));
    }

    function finalize() {
        fs.copyFile(outputFile, targetFile, () => {
            console.log(`File Exported as ${name}.${type}`);
            callback();
            rimraf(workingDir, EMPTY);
        });
    }
}


function runexport(options, res) {

    let url = options.url;
    let html = options.html;
    let type = safeType(options.type);
    let base = safeFilename(options.name, type);
    let slides = url;
    let dir = createTmpDir();
    let file = path.join(dir, base);

    if (html) {
        let htmlfile = `${file}.html`;
        fs.writeFileSync(htmlfile, html); 
        slides = htmlfile;
    }

    exportslides(slides, file, () => {
        res.setHeader('Content-disposition', 'attachment; filename=' + base );
        let filestream = fs.createReadStream(file);
        filestream.pipe(res);
    });

    res.on('finish', () => {
        rimraf(dir, EMPTY);
    })

}

function createpptx(filePath, options, callback) {
  
    const officegen = require('officegen');
    const pptx = officegen('pptx');
  
    const file = filePath || 'slides.pptx';
  
    options = options || {};
    const dir = options.dir || 'screenshots';  

    callback = typeof options === 'function' ? options : callback;
    callback = typeof callback === 'function' ? callback : EMPTY;
  
    const size = (options.size && options.size.includes('x')) ? options.size : '1280x720';
    const [width, height] = size.split('x').map(Number);
    pptx.setSlideSize (width, height);
    
    const title = options.title;
    if (title) pptx.setDocTitle(title);
  
    sortFiles(dir).forEach(img => {
          let imgpath = path.join(dir, img); 
          let slide = pptx.makeNewSlide();
          slide.addImage(imgpath, { x: 0, y: 0, cx: '100%', cy: '100%' });
    });
  
    const output = fs.createWriteStream(file);
    pptx.generate(output);
    output.on('error', console.log);
    output.on('close', callback);
  
}

function safeType(type) {
    if (type && !config.types.includes(type)) {
        throw `Please use one of the supported file extensions: ${config.types}`;
    }
    return type || 'pdf';
}

function safeFilename(name, type) {
    let safeName = name ? name.replace(/[^a-z0-9]/gi, '_') : 'slides';
    return type ? safeName + '.' + safeType(type) : safeName;
}

function sortFiles(dir) {
    return fs.readdirSync(dir)
            .map(function(v) { 
                return { 
                    name:v,
                    time:fs.statSync(path.resolve(dir, v)).mtime.getTime()
                }; 
            })
            .sort(function(a, b) { return a.time - b.time; })
            .map(function(v) { return v.name; });
}

function createTmpDir() {
    let randomString = Math.random().toString(36).substring(2, 15);
    let dir = path.join(os.tmpdir(), randomString);
    fs.mkdirSync(dir);
    return dir;
}

function EMPTY() {}



if (require.main === module) {
    exportslides(process.argv[2], process.argv[3]);
}

module.exports = {exportslides, runexport};
