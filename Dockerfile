FROM astefanutti/decktape

USER root

RUN apk update && apk upgrade && \
    apk add --no-cache \
        npm

RUN npm install -g \
        officegen \
        request \
        web-resource-inliner \
        website-scraper \
        archiver \
        rimraf \
        express \
        body-parser \
        pm2

ENV NODE_PATH=/usr/local/lib/node_modules

ADD . /exportslides

COPY ./exportslides.js /bin/exportslides

WORKDIR /exportslides

USER node

EXPOSE 5000

ENTRYPOINT [ "pm2-runtime", "server.js" ]
