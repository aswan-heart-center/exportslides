.PHONY: serve build shell down \
				clean clean-volumes help

image = exportslides
container = exportslides

serve: build
	docker run \
		--rm \
		-p 5000:5000 \
		--name $(container) \
		-v $(shell pwd):/exportslides \
		$(image)

build:
	docker build -t $(image) -f Dockerfile .

shell:
	docker exec -it $(container) /bin/sh

down:
	docker stop $(container)

restart: down server

clean:
	docker system prune

clean-volumes:
	docker system prune --volumes

help:
	@echo "Please use 'make <target>' where <target> is one of:"
	@echo ""
	@echo "  shell                   start interactive shell in container"
	@echo "  down                    stop and delete the container"
	@echo ""
	@echo "  clean                   clean unused docker containers, networks, images and cache"
	@echo "  clean-volumes           clean unused docker containers, networks, volumes, images and cache"
	@echo "  help                    show this list"
	@echo ""
	@echo "Check the Makefile to know exactly what each target is doing."
