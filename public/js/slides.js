function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

function getData() {
    let id = getUrlParameter('id');
    if (id) {
        let data = document.createElement('script');
        data.src = '/data/' + id;
        document.body.appendChild(data); 
    }
}

getData();

function render(item){
    let section = document.createElement('section');
    section.classList.add('reveal-section');
    document.getElementById("slides").appendChild(section); 
    
    let div = document.createElement('div');
    div.classList.add('slide-div');
    section.appendChild(div); 
    
    if (item.type == 'chart') {
        item.graph.config = { displayModeBar: false };
        item.graph.layout.font = {size: 16};
        div.classList.add('plotly-div');
        Plotly.newPlot(div, item.graph);

    } else if (item.type == 'stats'){
        div.classList.add('metric-container');

        item.metrics.forEach((metricItem) => {
            let field = document.createElement('div');
            field.classList.add('metric-field');
            div.appendChild(field); 
            
            let metric = document.createElement('div');
            metric.classList.add('metric');
            metric.innerHTML = metricItem.layout.annotations[0].text;
            field.appendChild(metric); 

        })

    } else if (item.type == 'text') {
            let h1 = document.createElement('h1');
            h1.innerHTML = item.content;
            div.appendChild(h1); 

    } else if (item.type == 'image') {
        let img = new Image();
        img.classList.add('fluid-img');        
        img.src = item.content;
        div.appendChild(img); 

    }

}

function resizePlots() {
    let divs = document.getElementsByClassName('plotly-div');
    for (let div of divs) {
        Plotly.relayout(div, {
            width: window.innerWidth,
            height: window.innerHeight,
        });
    }
};  
window.onresize = resizePlots;

window.onload = function() {

    data[0].content.forEach(render);
    resizePlots();

    Reveal.initialize({
        width: "100%",
        height: "100%",
        margin: 0,
        transition: "slide",
        minScale: 1,
        maxScale: 1,
        progress: false,
        controls: false,
});
} ;

