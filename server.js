const express = require('express');
const bodyParser = require('body-parser');

const {runexport} = require('./exportslides');

const app = express();
const port = 5000;

app.use(bodyParser.urlencoded({extended: false, limit: '10mb'}));

app.get('/', (req , res) => { 
    res.sendFile(__dirname + '/form.html');
});

app.get('/export', (req , res) => { 
    if (req.query.url) {
        const options = {
            url: req.query.url,
            type: req.query.type,
            name: req.query.name,
        };
        runexport(options, res);

    } else {
        res.sendFile(__dirname + '/error.html');
    }
});

app.post('/export', (req, res) => {
    if (req.body.html) {
        const options = {
            html: req.body.html,
            type: req.body.type,
            name: req.body.name,
        };
        runexport(options, res);

    } else {
        res.sendFile(__dirname + '/error.html');
    }
});

app.get('/data/:id', (req, res) => {
    let reportId = req.params.id;
    res.sendFile(__dirname + '/public/js/data.js');
});

app.use(express.static(__dirname + '/public'));

app.listen(port, () => console.log(`Server running on port ${port}!`));
